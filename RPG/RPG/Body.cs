﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Body : Armour
    {
        public Body(Stats stats, int levelRequirement, IArmourType.ArmourType armourType) : base(stats, levelRequirement, armourType)
        {

        }
    }
}
