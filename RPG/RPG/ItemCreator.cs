﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public static class ItemCreator
    {
        /// <summary>
        /// Create a new hero of a random class 
        /// </summary>
        /// <returns></returns>
        public static Hero CreateHero()
        {
            return CreateHero(RandomNumber.CreateRandomNumber(0, 4));
        }

        /// <summary>
        /// 0 = warrior, 1 = ranger, 2 = rogue, 3 = mage
        /// </summary>
        /// <param name="heroType"></param>
        /// <returns></returns>
        public static Hero CreateHero(int heroType)
        {
            switch (heroType)
            {
                case 0:
                    return new Warrior();
                    break;
                case 1:
                    return new Ranger();
                    break;
                case 2:
                    return new Rogue();
                    break;
                case 3:
                    return new Mage();
                    break;
                default:
                    //If an invalid value is given, it uses a random value.
                    return CreateHero();
                    break;
            }
        }

        /// <summary>
        /// Create a random weapon with a random level requirement up to the given value and the maximum value rolled stats can have
        /// </summary>
        /// <param name="maxLevel"></param>
        /// <param name="maxStat"></param>
        /// <returns></returns>
        public static Weapon CreateWeapon(int maxLevel, int maxStat)
        {
            IWeaponType.WeaponType weaponType = (IWeaponType.WeaponType)RandomNumber.CreateRandomNumber(0, Enum.GetValues(typeof(IWeaponType.WeaponType)).Length);
            return (CreateWeapon(weaponType, maxLevel, maxStat));
        }

        /// <summary>
        /// Create a weapon of specific type with a random level requirement up to the given value and the maximum value rolled stats can have
        /// </summary>
        /// <param name="weaponType"></param>
        /// <param name="maxLevel"></param>
        /// <param name="maxStat"></param>
        /// <returns></returns>
        public static Weapon CreateWeapon(IWeaponType.WeaponType weaponType, int maxLevel, int maxStat)
        {
            int levelRequirement = GenerateLevelRequirement(maxLevel);

            Stats stats = new Stats(GenerateStats(maxStat));

            int damage = RandomNumber.CreateRandomNumber(maxLevel, maxLevel * 3);
            float attackSpeed = RandomNumber.CreateRandomNumber(5, 30) / 10.0f;


            Weapon weapon = new Weapon(stats, levelRequirement, weaponType, damage, attackSpeed);
            return weapon;
        }

        /// <summary>
        /// Create a random piece of armour with a random level requirement up to the given value and the maximum value rolled stats can have
        /// </summary>
        /// <param name="maxLevel"></param>
        /// <param name="maxStat"></param>
        /// <returns></returns>
        public static Armour CreateArmour(int maxLevel, int maxStat)
        {
            int levelRequirement = GenerateLevelRequirement(maxLevel);

            Stats stats = new Stats(GenerateStats(maxStat));

            IArmourType.ArmourType armourType = (IArmourType.ArmourType)RandomNumber.CreateRandomNumber(0, Enum.GetValues(typeof(IArmourType.ArmourType)).Length);

            int number = RandomNumber.CreateRandomNumber(1, 3);
            switch (number)
            {
                case 1:
                    return new Head(stats, levelRequirement, armourType);
                    break; //Breaks here are not needed. It's mostly for consistency for myself.
                case 2:
                    return new Body(stats, levelRequirement, armourType);
                    break;
                case 3:
                    return new Legs(stats, levelRequirement, armourType);
                    break;
                default:
                    return new Armour(stats, levelRequirement, armourType);
                    break;
            }
        }

        /// <summary>
        /// Rolls each of the stats between 0 and the given maximum value. Returns an int[4] containing stats
        /// </summary>
        /// <param name="maxStat"></param>
        /// <returns></returns>
        static int[] GenerateStats(int maxStat)
        {

            int[] statValues = new int[4] { RandomNumber.CreateRandomNumber(0, maxStat), RandomNumber.CreateRandomNumber(0, maxStat), RandomNumber.CreateRandomNumber(0, maxStat), RandomNumber.CreateRandomNumber(0, maxStat) };
            return statValues;
        }

        /// <summary>
        /// Rolls the level requirement between 1 and the given maximum
        /// </summary>
        /// <param name="maxLevel"></param>
        /// <returns></returns>
        static int GenerateLevelRequirement(int maxLevel)
        {
            int levelRequirement;
            if (maxLevel <= 1)
            {
                levelRequirement = 1;
            }
            else
            {
                levelRequirement = RandomNumber.CreateRandomNumber(1, maxLevel);
            }
            return levelRequirement;
        }
    }
}
