﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Mage : Hero
    {
        public Mage(Stats baseStats, Stats levelUpStats) : base(baseStats, levelUpStats)
        {

        }

        public Mage() : base()
        {

        }

        /// <summary>
        /// Sets the stats to the base values
        /// </summary>
        override public void SetStats()
        {
            baseStats = new Stats(1, 1, 8, 5);
            totalStats = baseStats;
            levelUpStats = new Stats(1, 1, 5, 3);
            mainStat = Stats.StatName.Intelligence;
        }

        /// <summary>
        /// Sets the allowed armour types this hero may equip
        /// </summary>
        override public void SetAllowedArmourTypes()
        {
            allowedArmourTypes = new List<IArmourType.ArmourType>();
            allowedArmourTypes.Add(IArmourType.ArmourType.Cloth);
        }

        /// <summary>
        /// Sets the allowed weapon types this hero may equip
        /// </summary>
        override public void SetAllowedWeaponTypes()
        {
            allowedWeaponTypes = new List<IWeaponType.WeaponType>();
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Staff);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Wand);
        }
    }
}
