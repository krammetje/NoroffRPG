﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Armour : Equipment
    {
        public Armour(Stats stats, int levelRequirement, IArmourType.ArmourType armourType) : base(stats, levelRequirement)
        {
            this.armourType = armourType;
        }

        public IArmourType.ArmourType armourType;

        public IArmourType.ArmourType GetArmourType()
        {
            return armourType;
        }

        /// <summary>
        /// Returns the name of the item. If the item has no name, this will give the item a name
        /// </summary>
        /// <returns></returns>
        override public string GetName()
        {
            if (name == null || name.Length == 0)
            {
                name = NameGenerator.GenerateArmourName(this);
            }
            return name;
        }
    }
}
