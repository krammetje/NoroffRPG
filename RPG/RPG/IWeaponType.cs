﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public interface IWeaponType
    {
        public enum WeaponType 
        {
            Axe = 0,
            Bow = 1,
            Dagger = 2,
            Hammer = 3,
            Staff = 4,
            Sword = 5,
            Wand = 6
        }
    }
}
