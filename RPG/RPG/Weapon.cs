﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Weapon : Equipment
    {
        public Weapon(Stats stats, int levelRequirement, IWeaponType.WeaponType weaponType, int damage, float attacksPerSecond) : base(stats, levelRequirement)
        {
            this.damage = damage;
            this.attacksPerSecond = attacksPerSecond;
            this.weaponType = weaponType;
        }

        readonly int damage;
        readonly float attacksPerSecond;
        readonly IWeaponType.WeaponType weaponType;

        /// <summary>
        /// Returns the damage done per hit
        /// </summary>
        /// <returns></returns>
        public int GetWeaponDamage()
        {
            return damage;
        }

        /// <summary>
        /// Returns the number of attacks per second
        /// </summary>
        /// <returns></returns>
        public float GetAttackSpeed()
        {
            return attacksPerSecond;
        }

        /// <summary>
        /// Return the weapon type
        /// </summary>
        /// <returns></returns>
        public IWeaponType.WeaponType GetWeaponType()
        {
            return weaponType;
        }

        /// <summary>
        /// Returns the dps of this weapon (damage*speed)
        /// </summary>
        /// <returns></returns>
        public double GetDPS()
        {
            return (double)(damage * attacksPerSecond);
        }

        /// <summary>
        /// Returns the name of the weapon. If the weapon has no name, it generates a name.
        /// </summary>
        /// <returns></returns>
        override public string GetName()
        {
            if (name == null || name.Length == 0)
            {
                name = NameGenerator.GenerateWeaponName(this);
            }
            return name;
        }
    }
}
