﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public abstract class Hero
    {

        public Hero(Stats baseStats, Stats levelUpStats)
        {
            SetStats(baseStats, levelUpStats);
            UpdateSecondaries();
        }

        public Hero()
        {
            SetStats();
            UpdateSecondaries();
            SetAllowedArmourTypes();
            SetAllowedWeaponTypes();
        }

        public List<IWeaponType.WeaponType> allowedWeaponTypes;
        public List<IArmourType.ArmourType> allowedArmourTypes;

        public string name;
        public int level = 1;
        public Stats baseStats;
        public Stats totalStats;
        public SecondaryStats secondaryStats = new SecondaryStats();
        public Stats levelUpStats;
        public Stats.StatName mainStat;

        public Weapon equipedWeapon;
        public Body equippedBody;
        public Legs equippedLegs;
        public Head equippedHead;

        public void SetBaseStats(Stats stats)
        {
            baseStats.SetStat(stats.GetStat());
        }

        /// <summary>
        /// Override with correct base stats
        /// </summary>
        public virtual void SetStats()
        {
            this.baseStats.SetStat(new int[]{ 1,1,1,1});
            this.totalStats.SetStat(new int[] { 1, 1, 1, 1 });
            this.levelUpStats.SetStat(new int[] { 1, 1, 1, 1 });
            mainStat = Stats.StatName.Strength;
        }

        /// <summary>
        /// Override with correct armour types
        /// </summary>
        public virtual void SetAllowedArmourTypes()
        {
            allowedArmourTypes = new List<IArmourType.ArmourType>();
            allowedArmourTypes.Add(IArmourType.ArmourType.Cloth);
            allowedArmourTypes.Add(IArmourType.ArmourType.Leather);
            allowedArmourTypes.Add(IArmourType.ArmourType.Mail);
            allowedArmourTypes.Add(IArmourType.ArmourType.Plate);
        }


        /// <summary>
        /// Override with correct weapon types
        /// </summary>
        public virtual void SetAllowedWeaponTypes()
        {
            allowedWeaponTypes = new List<IWeaponType.WeaponType>();
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Axe);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Bow);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Dagger);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Hammer);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Staff);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Sword);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Wand);
        }

        public void SetStats(Stats stats, Stats levelUpStats)
        {
            this.baseStats = stats;
            this.totalStats = stats;
            this.levelUpStats = levelUpStats;
        }

        /// <summary>
        /// Increase level by 1
        /// </summary>
        public void LevelUp()
        {
            totalStats.IncreaseStat(levelUpStats.GetStat());
            UpdateSecondaries();
            level += 1;
        }

        /// <summary>
        /// Update secondary stats depending on the hero's primary stats
        /// </summary>
        public void UpdateSecondaries()
        {
            secondaryStats.UpdateSecondaries(totalStats);
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }

        /// <summary>
        /// Check if weapon may be equiped
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns></returns>
        public bool IsWeaponAllowed(Weapon weapon)
        {
            foreach(IWeaponType.WeaponType weapons in allowedWeaponTypes)
            {
                if(weapon.GetWeaponType() == weapons)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Check if armour may be equiped
        /// </summary>
        /// <param name="armour"></param>
        /// <returns></returns>
        public bool isArmourAllowed(Armour armour)
        {
            foreach(IArmourType.ArmourType armours in allowedArmourTypes)
            {
                if(armour.GetArmourType() == armours)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the dps of the weapon increased by 1% for each of the main stat. If no weapon is increased the base damage is 1.
        /// </summary>
        /// <returns></returns>
        public double GetDps()
        {
            double dps;
            if(equipedWeapon != null)
            {
                dps = (double)(equipedWeapon.GetDPS() * (1 + ((float)totalStats.GetStat(mainStat) / 100)));
            }
            else
            {
                dps = (double)(1 + ((float)totalStats.GetStat(mainStat) /100));
            }
            return dps;
        }

        /// <summary>
        /// Give hero item to equip. If hero can't equip item this writes an error.
        /// </summary>
        /// <param name="equipment"></param>
        public void EquipItem(Equipment equipment)
        {
            if(equipment.GetLevelRequirement() <= this.level)
            {
                //First check if equipment is a weapon since weapons have their own types
                if (equipment.GetType() == typeof(Weapon))
                {
                    if (IsWeaponAllowed((Weapon)equipment))
                    {
                        if (equipedWeapon != null)
                        {
                            LineWriter.WriteTextToUser(equipedWeapon.GetName() + " unequipped");
                            totalStats.DecreaseStats(equipedWeapon.GetStats().GetStat());
                        }
                        equipedWeapon = equipment as Weapon;
                        totalStats.IncreaseStat(equipment.GetStats().GetStat());
                        UpdateSecondaries();
                    }
                    else
                    {
                        LineWriter.EquipmentError(0);
                    }
                } else
                {
                    if (isArmourAllowed((Armour)equipment))
                    {
                        if (equipment.GetType() == typeof(Head))
                        {
                            if (equippedHead != null)
                            {
                                LineWriter.WriteTextToUser(equippedHead.GetName() + " unequipped");
                                totalStats.DecreaseStats(equippedHead.GetStats().GetStat());
                            }
                            equippedHead = equipment as Head;
                            totalStats.IncreaseStat(equipment.GetStats().GetStat());
                        }

                        if (equipment.GetType() == typeof(Body))
                        {
                            if (equippedBody != null)
                            {
                                LineWriter.WriteTextToUser(equippedBody.GetName() + " unequipped");
                                totalStats.DecreaseStats(equippedBody.GetStats().GetStat());
                            }
                            equippedBody = equipment as Body;
                            totalStats.IncreaseStat(equipment.GetStats().GetStat());
                        }

                        if (equipment.GetType() == typeof(Legs))
                        {
                            if (equippedLegs != null)
                            {
                                LineWriter.WriteTextToUser(equippedLegs.GetName() + " unequipped");
                                totalStats.DecreaseStats(equippedLegs.GetStats().GetStat());
                            }
                            equippedLegs = equipment as Legs;
                            totalStats.IncreaseStat(equipment.GetStats().GetStat());
                        }
                        totalStats.IncreaseStat(equipment.GetStats().GetStat());
                        UpdateSecondaries();
                        LineWriter.WriteTextToUser(equipment.GetName() + " equipped");
                        LineWriter.WriteTextToUser("New stats:");
                        LineWriter.WriteStats(this);
                    }
                    else
                    {
                        LineWriter.EquipmentError(0);
                    }
                }
            }
            else
            {
                LineWriter.EquipmentError(1);
            }
        }
    }
}
