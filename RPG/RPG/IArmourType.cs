﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public interface IArmourType
    {
        public enum ArmourType
        {
            Cloth = 0,
            Leather = 1,
            Mail = 2,
            Plate = 3
        }
    }
}
