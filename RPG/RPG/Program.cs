﻿using System;
using System.Collections.Generic;

namespace RPG
{
    class Program
    {
        static void Main(string[] args)
        {
            GameStart();
        }

        static void GameStart()
        {
            Hero hero = CreateHero();
            LineWriter.WriteEmptyLine();
            LineWriter.WriteTextToUser($"You are {hero.GetName()} the {hero.GetType().Name}");
            LineWriter.WriteStats(hero);
            LineWriter.WriteEmptyLine();

            Weapon weapon = CreateStarterWeapon(hero);
            LineWriter.WriteTextToUser($"Before you start your adventure, you are given {weapon.GetName()} as your starting weapon.");
            LineWriter.WriteStats(weapon);
            if(LineWriter.AskQuestionYN("Do you wish to equip your starter weapon?"))
            {
                LineWriter.WriteEmptyLine();
                hero.EquipItem(weapon);
                LineWriter.WriteTextToUser(weapon.GetName() + " equipped");
                LineWriter.WriteTextToUser("You equip your new weapon. Your stats have been updated.");
                LineWriter.WriteStats(hero);
                LineWriter.WriteEmptyLine();
            }

            LineWriter.WriteTextToUser("Now it's time to start your adventure. Disclaimer, there is no actual gameplay.");
            Explore(hero);
        }

        static Hero CreateHero()
        {
            Hero hero;
            LineWriter.WriteTextToUser("Creating new hero");
            string characterChoice = LineWriter.AskQuestion("What class would you like to play? (Warrior/Ranger/Rogue/Mage) Leave empty to get a random character.");
            LineWriter.WriteEmptyLine();
            switch (characterChoice.ToLower())
            {
                case "warrior":
                    hero = ItemCreator.CreateHero(0);
                    break;
                case "ranger":
                    hero = ItemCreator.CreateHero(1);
                    break;
                case "rogue":
                    hero = ItemCreator.CreateHero(2);
                    break;
                case "mage":
                    hero = ItemCreator.CreateHero(3);
                    break;
                default:
                    hero = ItemCreator.CreateHero();
                    break;
            }
            if(LineWriter.AskQuestionYN("Do you want a random name for your hero?")){
                hero.SetName(NameGenerator.GenerateName());
            }
            else
            {
                hero.SetName(LineWriter.AskQuestion("What name would you like to give your hero?", true));
            }
            return hero;
        }

        static Weapon CreateStarterWeapon(Hero hero)
        {
            List<IWeaponType.WeaponType> weapons = hero.allowedWeaponTypes;
            IWeaponType.WeaponType weaponType = weapons[RandomNumber.CreateRandomNumber(0, weapons.Count)];
            return ItemCreator.CreateWeapon(weaponType, 1, 2);
        }

        static void Train(Hero hero)
        {
            LineWriter.WriteTextToUser("While exploring you gained a level.");
            LineWriter.WriteEmptyLine();
            LineWriter.WriteTextToUser($"{hero.name} went from level {hero.level}");
            LineWriter.WriteStats(hero);
            hero.LevelUp();
            LineWriter.WriteEmptyLine();
            LineWriter.WriteTextToUser($"To level {hero.level}");
            LineWriter.WriteStats(hero);
        }

        static void FindItem(Hero hero, Equipment equipment)
        {
            LineWriter.WriteTextToUser($"{hero.GetName()} found an item: {equipment.GetName()}");
            LineWriter.WriteStats(equipment.levelRequirement, equipment.stats);
            if(LineWriter.AskQuestionYN($"Do you wish to equip {equipment.GetName()}")){
                hero.EquipItem(equipment);
            }
        }

        static void Explore(Hero hero)
        {
            switch(RandomNumber.CreateRandomNumber(0, 4))
            {
                case 0:
                    FindItem(hero, ItemCreator.CreateArmour(hero.level + 2, hero.level + 3));
                    break;
                case 1:
                    FindItem(hero, ItemCreator.CreateWeapon(hero.level + 2, hero.level + 3));
                    break;
                case 2:
                    Train(hero);
                    break;
                case 3:
                    LineWriter.WriteTextToUser("Nothing remarkable happened on your adventure today");
                    break;

            }

            LineWriter.WriteEmptyLine();
            if(LineWriter.AskQuestionYN("Do you want to go on another adventure?")){
                Explore(hero);
            }
        }
    }
}
