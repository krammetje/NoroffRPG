﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Ranger : Hero
    {
        public Ranger(Stats baseStats, Stats levelUpStats) : base(baseStats, levelUpStats)
        {
            
        }

        public Ranger() : base()
        {

        }

        /// <summary>
        /// Sets the stats to the base values
        /// </summary>
        override public void SetStats()
        {
            baseStats = new Stats(1, 7, 1, 8);
            totalStats = baseStats;
            levelUpStats = new Stats(1, 5, 1, 2);
            mainStat = Stats.StatName.Dexterity;
        }

        /// <summary>
        /// Sets the allowed armour types this hero may equip
        /// </summary>
        override public void SetAllowedArmourTypes()
        {
            allowedArmourTypes = new List<IArmourType.ArmourType>();
            allowedArmourTypes.Add(IArmourType.ArmourType.Leather);
            allowedArmourTypes.Add(IArmourType.ArmourType.Mail);
        }

        /// <summary>
        /// Sets the allowed weapon types this hero may equip
        /// </summary>
        override public void SetAllowedWeaponTypes()
        {
            allowedWeaponTypes = new List<IWeaponType.WeaponType>();
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Bow);
        }
    }
}
