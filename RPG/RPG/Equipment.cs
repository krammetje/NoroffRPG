﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public abstract class Equipment
    {
        public Equipment(Stats stats, int levelRequirement)
        {
            this.stats = stats;
            this.levelRequirement = levelRequirement;
        }

        public string name;
        public int levelRequirement;
        public Stats stats;

        public Stats GetStats()
        {
            return stats;
        }

        public int GetLevelRequirement()
        {
            return levelRequirement;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Should be overwritten by children. If not overridden and the item has no name, it will return 'Unknown item'
        /// </summary>
        /// <returns></returns>
        virtual public string GetName()
        {
            if (name == null || name.Length == 0)
            {
                return "Unknown Item";
            }
            return name;
        }
    }

}
