﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public static class LineWriter
    {
        /// <summary>
        /// Writes stats in format [lvl | dps | str | dex | int | vit | hp | arm | res]
        /// </summary>
        /// <param name="statsToWrite"></param>
        /// <param name="secondaryStatsToWrite"></param>
        public static void WriteStats(int level, Stats statsToWrite, SecondaryStats secondaryStatsToWrite, double dps)
        {
            int[] statArray = statsToWrite.GetStat();
            int[] secondaryStatArray = secondaryStatsToWrite.GetStats();
            Console.WriteLine($"Stats [Lvl: {level} | Dps: {String.Format("{0:0.0}",dps)} | Str: {statArray[0]} | Dex: {statArray[1]} | Int {statArray[2]} | Vit {statArray[3]} | HP {secondaryStatArray[0]} | Arm {secondaryStatArray[1]} | Res {secondaryStatArray[2]}]");
        }

        /// <summary>
        /// Writes stats in format [lvl | str | dex | int | vit]
        /// </summary>
        /// <param name="statsToWrite"></param>
        /// <param name="levelRequirement"></param>
        public static void WriteStats(int levelRequirement, Stats statsToWrite)
        {
            int[] statArray = statsToWrite.GetStat();
            Console.WriteLine($"Stats [Lvl: {levelRequirement} | Str: {statArray[0]} | Dex: {statArray[1]} | Int {statArray[2]} | Vit {statArray[3]}]");
        }

        public static void WriteStats(Weapon weapon)
        {
            int[] statArray = weapon.GetStats().GetStat();
            int damage = weapon.GetWeaponDamage();
            int levelRequirement = weapon.GetLevelRequirement();
            float speed = weapon.GetAttackSpeed();
            double dps = weapon.GetDPS();
            Console.WriteLine($"Stats [Lvl: {levelRequirement} | Dps: {String.Format("{0:0.0}", dps)} | Dmg: {damage} | Spd: {speed} | Str: {statArray[0]} | Dex: {statArray[1]} | Int {statArray[2]} | Vit {statArray[3]}]");
        }

        /// <summary>
        /// Writes stats in format [lvl | str | dex | int | vit | hp | arm | res]
        /// </summary>
        /// <param name="hero"></param>
        public static void WriteStats(Hero hero)
        {
            WriteStats(hero.level, hero.totalStats, hero.secondaryStats, hero.GetDps());
        }

        /// <summary>
        /// WriteLine with string
        /// </summary>
        /// <param name="message"></param>
        public static void WriteTextToUser(string message)
        {
            Console.WriteLine(message);
        }

        /// <summary>
        /// write an error depending on the integer entered.
        /// 0=wrong class
        /// 1=level requirement
        /// </summary>
        /// <param name="message"></param>
        public static void EquipmentError(int message)
        {
            string error;
            switch (message)
            {
                case 0:
                    error = "Your class can not equip this item.";
                    break;
                case 1:
                    error = "Your level is to low to equip this item.";
                    break;
                default:
                    error = "Reason unkown.";
                    break;
            }
            WriteTextToUser($"Unable to equip item: {error}");
        }

        /// <summary>
        /// Write a question to the user, returns the answer.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string AskQuestion(string message)
        {
            Console.WriteLine(message);
            return (Console.ReadLine());
        }

        /// <summary>
        /// Write a question to the user, returns the answer. Checks if the entry is empty.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cantBeEmpty"></param>
        /// <returns></returns>
        public static string AskQuestion(string message, bool cantBeEmpty)
        {
            Console.WriteLine(message);
            string temp = Console.ReadLine();
            if (cantBeEmpty)
            {
                while (temp.Length == 0)
                {
                    temp = Console.ReadLine();
                }
            }
            return temp;
        }

        /// <summary>
        /// Returns true if yes is entered, or false when no is entered. empty will be seen as yes
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool AskQuestionYN(string message)
        {
            Console.WriteLine(message + " (Y/N)");
            string temp = Console.ReadLine();
            switch (temp.ToLower())
            {
                case "y":
                case "yes":
                case "":
                    return true;
                    break;
                case "n":
                case "no":
                    return false;
                    break;
                default:
                    return AskQuestionYN("Please enter a valid answer.");
                    break;
            }
        }

        public static void WriteEmptyLine()
        {
            Console.WriteLine("");
        }
    }
}
