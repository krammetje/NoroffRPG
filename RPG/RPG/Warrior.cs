﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Warrior : Hero
    {
        public Warrior(Stats baseStats, Stats levelUpStats) : base(baseStats, levelUpStats)
        {
            
        }

        public Warrior() : base()
        {
            
        }

        /// <summary>
        /// Sets the stats to the base values
        /// </summary>
        override public void SetStats()
        {
            baseStats = new Stats(5, 2, 1, 10);
            totalStats = baseStats;
            levelUpStats = new Stats(3, 2, 1, 5);
            mainStat = Stats.StatName.Strength;
        }

        /// <summary>
        /// Sets the allowed armour types this hero may equip
        /// </summary>
        override public void SetAllowedArmourTypes()
        {
            allowedArmourTypes = new List<IArmourType.ArmourType>();
            allowedArmourTypes.Add(IArmourType.ArmourType.Mail);
            allowedArmourTypes.Add(IArmourType.ArmourType.Plate);
        }


        /// <summary>
        /// Sets the allowed weapon types this hero may equip
        /// </summary>
        override public void SetAllowedWeaponTypes()
        {
            allowedWeaponTypes = new List<IWeaponType.WeaponType>();
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Axe);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Hammer);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Sword);
        }
    }
}
