﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Head : Armour
    {
        public Head(Stats stats, int levelRequirement, IArmourType.ArmourType armourType) : base(stats, levelRequirement, armourType)
        {

        }
    }
}
