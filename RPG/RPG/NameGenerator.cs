﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public static class NameGenerator
    {
        static string[] firstName = new string[]
        {
            "John",
            "Pete",
            "Morty",
            "Steve",
            "Odin",
            "Odis",
            "Hera",
            "Maximus",
            "Alyn",
            "Phudor",
            "Evys",
            "Ruby",
            "Jade",
            "Sapphire"
        };

        static string[] surnamePrefix = new string[]
        {
            "Ruby",
            "Berry",
            "Ice",
            "Chalk",
            "Patch",
            "Fiddle",
            "Bunny",
            "Sugar",
            "Puzzle",
            "Fluffy",
            "Rage",
            "Blade",
            "Fire"
        };

        static string[] surnameSuffix = new string[]
        {
            "rage",
            "bottom",
            "skull",
            "flame",
            "patches",
            "loop",
            "fur",
            "ruby"
        };

        static string[] helmetType = new string[]
        {
            "full helm",
            "circlet",
            "sallet",
            "great helm",
            "bascinet",
            "hood"
        };

        static string[] armourSuffix = new string[]
        {
            "of Fire",
            "of the North",
            "of the General Store",
            "of Goblinkind"
        };

        static string[] weaponPrefix = new string[]
        {
            "Burning",
            "Frozen",
            "Dull",
            "Sharp",
            "Massive",
            "Twin",
            "The archmage's",
            "Cardboard",
            "Annoying",
            "Laughing",
            "Animated"
        };

        static string[] weaponSuffix = new string[]
        {
            "of Evil",
            "of the Woods",
            "of duality",
            "of the flames",
            "of cold",
            "of shadows"
        };

        /// <summary>
        /// Returns a randomly generated hero name.
        /// </summary>
        /// <returns></returns>
        public static string GenerateName()
        {
            return
                firstName[RandomNumber.CreateRandomNumber(0, firstName.Length)] + " " +
                surnamePrefix[RandomNumber.CreateRandomNumber(0, surnamePrefix.Length)] +
                surnameSuffix[RandomNumber.CreateRandomNumber(0, surnameSuffix.Length)];
        }

        /// <summary>
        /// Create a random generated name for a piece of armour using the item type, armour type and list of suffixes.
        /// </summary>
        /// <param name="armour"></param>
        /// <returns></returns>
        public static string GenerateArmourName(Armour armour)
        {
            string itemName;

            if (armour.GetType() == typeof(Head))
            {
                itemName = armour.GetArmourType().ToString() + " " + helmetType[RandomNumber.CreateRandomNumber(0,helmetType.Length)] + " " + armourSuffix[RandomNumber.CreateRandomNumber(0, armourSuffix.Length)];
            }
            else
            {
                itemName = armour.GetArmourType().ToString() + " " + armour.GetType().Name.ToLower() + " " + armourSuffix[RandomNumber.CreateRandomNumber(0, armourSuffix.Length)];
            }
            return itemName;
        }

        /// <summary>
        /// Create a random generated name for a weapon using the weapon type and a list of prefixes and suffixes
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns></returns>
        public static string GenerateWeaponName(Weapon weapon)
        {
            string weaponType = weapon.GetWeaponType().ToString().ToLower();
            return weaponPrefix[RandomNumber.CreateRandomNumber(0, weaponPrefix.Length)] + " " + weaponType + " " + weaponSuffix[RandomNumber.CreateRandomNumber(0, weaponSuffix.Length)];
        }
    }
}
