﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public static class RandomNumber
    {

        /// <summary>
        /// Returns a random number between 0 (included) and given maximum (excluded)
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int CreateRandomNumber(int max)
        {
            Random random = new Random();
            return random.Next(max);
        }

        /// <summary>
        /// Returns a random number between minimum (included) and maximum (excluded)
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int CreateRandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
    }
}
