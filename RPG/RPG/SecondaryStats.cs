﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class SecondaryStats
    {
        public enum StatName
        {
            Health = 0,
            Armor = 1,
            Resistances = 2
        }

        private int[] secondaryStats = new int[] { 0, 0, 0 };


        public void UpdateSecondaries(Stats stats)
        {
            secondaryStats[0] = stats.GetStat(Stats.StatName.Vitality) * 10;
            secondaryStats[1] = stats.GetStat(Stats.StatName.Dexterity) + stats.GetStat(Stats.StatName.Strength);
            secondaryStats[2] = stats.GetStat(Stats.StatName.Intelligence);
        }

        public int[] GetStats()
        {
            return secondaryStats;
        }
    }
}
