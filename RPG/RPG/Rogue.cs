﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Rogue : Hero
    {
        public Rogue(Stats baseStats, Stats levelUpStats) : base(baseStats, levelUpStats)
        {

        }

        public Rogue() : base()
        {
            
        }

        /// <summary>
        /// Sets the stats to the base values
        /// </summary>
        override public void SetStats()
        {
            baseStats = new Stats(2, 6, 1, 8);
            totalStats = baseStats;
            levelUpStats = new Stats(1, 4, 1, 3);
            mainStat = Stats.StatName.Dexterity;
        }

        /// <summary>
        /// Sets the allowed armour types this hero may equip
        /// </summary>
        override public void SetAllowedArmourTypes()
        {
            allowedArmourTypes = new List<IArmourType.ArmourType>();
            allowedArmourTypes.Add(IArmourType.ArmourType.Leather);
            allowedArmourTypes.Add(IArmourType.ArmourType.Mail);
        }

        /// <summary>
        /// Sets the allowed weapon types this hero may equip
        /// </summary>
        override public void SetAllowedWeaponTypes()
        {
            allowedWeaponTypes = new List<IWeaponType.WeaponType>();
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Dagger);
            allowedWeaponTypes.Add(IWeaponType.WeaponType.Sword);   
        }
    }
}
