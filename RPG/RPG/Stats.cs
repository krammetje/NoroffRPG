﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Stats
    {
        public Stats(int strength, int dexterity, int intelligence, int vitality)
        {
            int[] stats = new int[4] { strength, dexterity, intelligence, vitality };
            SetStat(stats);
        }

        public Stats (int[] stats)
        {
            SetStat(stats);
        }

        public enum StatName { 
        Strength = 0,
        Dexterity = 1,
        Intelligence = 2,
        Vitality = 3
        }
        private int[] stats = new int[] { 1, 1, 1, 1};
        public int[] Stat { get => stats; set => stats = value; }
        
        /// <summary>
        /// Returns a specific stat
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        public int GetStat(StatName stat)
        {
            return (stats[(int)stat]);
        }

        /// <summary>
        /// Returns int[4] stats
        /// </summary>
        /// <returns></returns>
        public int[] GetStat()
        {
            return stats;
        }

        /// <summary>
        /// Set specific stat to given value
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="value"></param>
        public void SetStat(StatName stat, int value)
        {
            stats[(int)stat] = value;
        }

        /// <summary>
        /// Set stats to given values
        /// </summary>
        /// <param name="values"></param>
        public void SetStat(int[] values)
        {
            if (values.Length >= 4)
            {
                for(int i = 0; i<4; i++)
                {
                    stats[i] = values[i];
                }
            }
            else
            {
                Console.WriteLine("An error occured while setting the stats. This action requires 4 attributes.");
            }
        }
        
        /// <summary>
        /// Increases specific stat by given amount
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="value"></param>
        public void IncreaseStat(StatName stat, int value)
        {
            stats[(int)stat] += value;
        }

        /// <summary>
        /// Increases stats by gives values
        /// </summary>
        /// <param name="values"></param>
        public void IncreaseStat(int[] values)
        {
            if (values.Length >= 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    stats[i] += values[i];
                }
            }
            else
            {
                Console.WriteLine("An error occured while increasing the stats. This action requires 4 attributes.");
            }
        }

        /// <summary>
        /// Reduces specific stat by given amount
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="value"></param>
        public void DecreaseStats(StatName stat, int value)
        {
            IncreaseStat(stat, -value);
        }

        /// <summary>
        /// Reduces the stats by given values
        /// </summary>
        /// <param name="values"></param>
        public void DecreaseStats(int[] values)
        {
            int[] temp = new int[values.Length];
            for(int i = 0; i < values.Length; i++)
            {
                temp[i] = -values[i];
            }
            IncreaseStat(temp);
        }
    }
}
