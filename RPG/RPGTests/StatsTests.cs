﻿using System;
using Xunit;

namespace RPGTests
{
    public class StatsTests
    {
        [Fact]
        public void IncreaseStats_IncreaseStatsByValues_IncreasedStats()
        {
            //Arrange
            RPG.Stats stats = new RPG.Stats(4, 3, 2, 1);
            int[] add = new int[4] { 1, 2, 3, 4 };
            RPG.Stats expected = new RPG.Stats(
                stats.GetStat(RPG.Stats.StatName.Strength) + add[0],
                stats.GetStat(RPG.Stats.StatName.Dexterity) + add[1],
                stats.GetStat(RPG.Stats.StatName.Intelligence) + add[2],
                stats.GetStat(RPG.Stats.StatName.Vitality) + add[3]
                );

            //Act
            stats.IncreaseStat(add);
            RPG.Stats actual = stats;

            //Assert
            Assert.Equal<int[]>(expected.GetStat(), actual.GetStat());
        }
    }
}
