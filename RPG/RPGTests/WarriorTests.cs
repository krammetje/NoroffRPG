﻿using System;
using System.IO;
using Xunit;

namespace RPGTests
{
    public class WarriorTests
    {
        [Fact]
        public void EquipItem_EquipWeapon_ReturnTotalStats()
        {
            //Arrange
            RPG.Warrior warrior = new RPG.Warrior();
            RPG.Stats stats = new RPG.Stats(4, 3, 1, 1);
            RPG.Weapon weapon = new RPG.Weapon(stats, 1, RPG.IWeaponType.WeaponType.Axe, 2, 1);
            int[] expected = new int[4] {
                warrior.totalStats.GetStat(RPG.Stats.StatName.Strength) + weapon.GetStats().GetStat(RPG.Stats.StatName.Strength),
                warrior.totalStats.GetStat(RPG.Stats.StatName.Dexterity) + weapon.GetStats().GetStat(RPG.Stats.StatName.Dexterity),
                warrior.totalStats.GetStat(RPG.Stats.StatName.Intelligence) + weapon.GetStats().GetStat(RPG.Stats.StatName.Intelligence),
                warrior.totalStats.GetStat(RPG.Stats.StatName.Vitality) + weapon.GetStats().GetStat(RPG.Stats.StatName.Vitality)
            };
            //Act
            warrior.EquipItem(weapon);
            int[] actual = warrior.totalStats.GetStat();

            //Assert
            Assert.Equal<int[]>(expected, actual);
        }

        [Fact]
        public void EquipItem_WrongBaseType_OutputErrorMessage()
        {
            //Arrange
            RPG.Warrior warrior = new RPG.Warrior();
            RPG.Stats stats = new RPG.Stats(4, 3, 1, 1);
            RPG.Weapon weapon = new RPG.Weapon(stats, 1, RPG.IWeaponType.WeaponType.Wand, 2, 1);

            StringWriter stringWriter = new StringWriter();  //This will be used instead of a console
            TextWriter originalOutput = Console.Out;
            Console.SetOut(stringWriter);

            RPG.LineWriter.EquipmentError(0); //This is used to check if the error message is shown. When writing this out as a string, it fails due to linebreaks missing.
            string expected = stringWriter.ToString();
            stringWriter = new StringWriter(); //Gives actual a clean field to write from, avoid errors due to previous lines being used.
            Console.SetOut(stringWriter);

            //Act
            warrior.EquipItem(weapon);
            string actual = stringWriter.ToString();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_WeaponRequirementToHigh_OutputErrorMessage()
        {
            //Arrange
            RPG.Warrior warrior = new RPG.Warrior();
            RPG.Stats stats = new RPG.Stats(4, 3, 1, 1);
            RPG.Weapon weapon = new RPG.Weapon(stats, 99, RPG.IWeaponType.WeaponType.Axe, 2, 1);

            StringWriter stringWriter = new StringWriter();  //This will be used instead of a console
            TextWriter originalOutput = Console.Out;
            Console.SetOut(stringWriter);

            RPG.LineWriter.EquipmentError(1); //This is used to check if the error message is shown. When writing this out as a string, it fails due to linebreaks missing.
            string expected = stringWriter.ToString();
            stringWriter = new StringWriter(); //Gives actual a clean field to write from, avoid errors due to previous lines being used.
            Console.SetOut(stringWriter);

            //Act
            warrior.EquipItem(weapon);
            string actual = stringWriter.ToString();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDps_GetDpsNoItems_ReturnDPS()
        {
            //Arrange
            RPG.Warrior warrior = new RPG.Warrior();
            double expected = (double)(1 + ((float)warrior.totalStats.GetStat(RPG.Stats.StatName.Strength)/100));

            //Act
            double actual = warrior.GetDps();

            bool diff = (Math.Abs(expected - actual) < Single.Epsilon);

            //Assert
            Assert.True(diff);
        }

        [Fact]
        public void GetDps_GetDpsWeaponEquipped_ReturnDPS()
        {
            //Arrange
            RPG.Warrior warrior = new RPG.Warrior();
            RPG.Stats stats = new RPG.Stats(4, 3, 1, 1);
            RPG.Weapon weapon = new RPG.Weapon(stats, 1, RPG.IWeaponType.WeaponType.Axe, 2, 1);
            warrior.EquipItem(weapon);

            double expected = weapon.GetDPS() * (double)(1 + ((float)warrior.totalStats.GetStat(RPG.Stats.StatName.Strength) / 100));

            //Act
            double actual = warrior.GetDps();

            bool diff = (Math.Abs(expected - actual) < Single.Epsilon);

            //Assert
            Assert.True(diff);
        }

        [Fact]
        public void GetDps_GetDPSWeaponAndArmourEquipped_ReturnDPS()
        {
            //Arrange
            RPG.Warrior warrior = new RPG.Warrior();
            
            RPG.Stats weaponStats = new RPG.Stats(4, 3, 1, 1);
            RPG.Weapon weapon = new RPG.Weapon(weaponStats, 1, RPG.IWeaponType.WeaponType.Axe, 2, 1);
            warrior.EquipItem(weapon);

            RPG.Stats armourStats = new RPG.Stats(12, 28, 0, 5);
            RPG.Armour body = new RPG.Body(armourStats, 1, RPG.IArmourType.ArmourType.Plate);
            warrior.EquipItem(body);

            double expected = weapon.GetDPS() * (double)(1 + ((float)warrior.totalStats.GetStat(RPG.Stats.StatName.Strength) / 100));

            //Act
            double actual = warrior.GetDps();

            bool diff = (Math.Abs(expected - actual) < Single.Epsilon);

            //Assert
            Assert.True(diff);
        }

        [Fact]
        public void EquipItem_EquipableArmour_MessageConfirm()
        {
            //Arrange
            RPG.Warrior warrior = new RPG.Warrior();

            RPG.Stats stats = new RPG.Stats(2, 2, 2, 5);
            RPG.Armour armour = new RPG.Armour(stats, 1, RPG.IArmourType.ArmourType.Plate);
            RPG.Stats newStats = new RPG.Stats(
                warrior.totalStats.GetStat(RPG.Stats.StatName.Strength) + stats.GetStat(RPG.Stats.StatName.Strength),
                warrior.totalStats.GetStat(RPG.Stats.StatName.Dexterity) + stats.GetStat(RPG.Stats.StatName.Dexterity),
                warrior.totalStats.GetStat(RPG.Stats.StatName.Intelligence) + stats.GetStat(RPG.Stats.StatName.Intelligence),
                warrior.totalStats.GetStat(RPG.Stats.StatName.Vitality) + stats.GetStat(RPG.Stats.StatName.Vitality));
            warrior.totalStats = newStats;
            warrior.UpdateSecondaries();
            armour.SetName("TestItem");

            StringWriter stringWriter = new StringWriter();  //This will be used instead of a console
            TextWriter originalOutput = Console.Out;
            Console.SetOut(stringWriter);

            RPG.LineWriter.WriteTextToUser(armour.GetName() + " equipped");
            RPG.LineWriter.WriteTextToUser("New stats:");
            RPG.LineWriter.WriteStats(warrior);
            string expected = stringWriter.ToString();

            stringWriter = new StringWriter(); //Gives actual a clean field to write from, avoid errors due to previous lines being used.
            Console.SetOut(stringWriter);

            //Act
            warrior = new RPG.Warrior();
            warrior.EquipItem(armour);
            string actual = stringWriter.ToString();

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
